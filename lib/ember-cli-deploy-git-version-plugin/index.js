const cpr = require("cpr");
const fs = require("fs-extra");
const Promise = require("rsvp").Promise;
const DeployPluginBase = require("ember-cli-deploy-plugin");
/* eslint-env node */
("use strict");

const vendorRegex = RegExp(
  /(.*?)(?:^|\b)(seismic-core-css-)(?=\b|$)(.*?)(?:^|\b)(.css)(?=\b|$)/
);

module.exports = {
  name: "ember-cli-deploy-git-version-plugin",

  isDevelopingAddon() {
    return true;
  },
  createDeployPlugin(options) {
    const DeployPlugin = DeployPluginBase.extend({
      name: options.name,
      defaultConfig: {
        distDir(context) {
          return context.distDir;
        },
        destDir(context) {
          return context.destDir;
        }
      },
      upload(context) {
        var distDir = this.readConfig("distDir");
        var destDir = this.readConfig("destDir");
        var version = context.project.pkg.version;

        if (!fs.existsSync(destDir)) {
          fs.mkdirSync(destDir);
        }

        const regex = RegExp(
          /(.*?)(?:^|\b)(seismic-core-css-)(?=\b|$)(.*?)(?:^|\b)(.css)(?=\b|$)/
        );
        const css = context.distFiles.filter(file => regex.test(file));
        const original = `${distDir}/${css[0]}`;
        const copied = `${distDir}/seismic-atomic-css-${version}.css`;

        fs.copySync(original, copied);
      }
    });
    return new DeployPlugin();
  }
};
