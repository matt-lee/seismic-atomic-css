import Component from "@ember/component";
/**
 * A component for displaying a heading in MeetingSpace
 *
 *      {{ui/-heading label="Sample"}}
 *
 *
 *      {{#ui/-heading label="sample" class="sample"}}
 *        Yielded Content
 *      {{/ui-heading}}
 *
 * @class -heading
 * @augments ember/Component
 */
export default Component.extend({
  /**
   * @property classNames
   * @type Array
   * @default ["border-b flex mb-16"]
   */
  classNames: ["border-b flex my-16"]
});
