import Route from '@ember/routing/route';

export default Route.extend({
  model() {
    const tailwind = this.modelFor('docs');

    return {
      typography: tailwind['typography'],
      tracking: tailwind['tracking'],
      fonts: tailwind['fonts'],
      fontWeights: tailwind['fontWeights']
    };
  }
});
