import Route from '@ember/routing/route';

export default Route.extend({
  model() {
    const tailwind = this.modelFor('docs');

    return {
      width: tailwind['width'],
      height: tailwind['height'],
      minWidth: tailwind['minWidth'],
      maxWidth: tailwind['maxWidth'],
      minHeight: tailwind['minHeight'],
      maxHeight: tailwind['maxHeight']
    };
  }
});
