import Route from '@ember/routing/route';

export default Route.extend({
  model() {
    const tailwind = this.modelFor('docs');

    return {
      borderColors: tailwind['borderColors'],
      borderRadius: tailwind['borderRadius'],
      borderWidths: tailwind['borderWidths']
    };
  }
});
