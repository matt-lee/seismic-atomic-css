import Route from '@ember/routing/route';

export default Route.extend({
  model() {
    const tailwind = this.modelFor('docs');

    return {
      colors: tailwind['colors'],
      textColors: tailwind['textColors'],
      bgColors: tailwind['bgColors']
    };
  }
});
