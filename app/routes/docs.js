import Route from '@ember/routing/route';
import tailwind from 'seismic-core-css/tailwind/config/tailwind';

export default Route.extend({
  model() {
    return tailwind;
  }
});
