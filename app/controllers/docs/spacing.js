import Controller from '@ember/controller';
import { computed } from '@ember/object';
import grid from 'seismic-core-css/tailwind/config/grid';

export default Controller.extend({
  css: 'm',
  size: '16',
  side: '',
  display: computed('side', 'size', 'css', function() {
    const { side, size, css } = this.getProperties('side', 'size', 'css');
    return `${css}${side}-${size}`;
  }),
  classes: [
    { label: 'Padding', key: 'p' },
    { label: 'Margin', key: 'm' },
    { label: 'Negative Margin', key: '-m' }
  ],
  sides: [
    { label: 'All', key: '' },
    { label: 'Vertial', key: 'y' },
    { label: 'Horizonal', key: 'x' },
    { label: 'Top', key: 't' },
    { label: 'Right', key: 'r' },
    { label: 'Bottom', key: 'b' },
    { label: 'Left', key: 'l' }
  ],
  sizes: grid
});
