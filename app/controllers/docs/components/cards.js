import Controller from '@ember/controller';
import { computed } from '@ember/object';

export default Controller.extend({
  length: 3,
  cards: computed('length', function() {
    let results = [];
    for (var i = 0; i < this.get('length'); i++) {
      results.push({});
    }

    return results;
  })
});
