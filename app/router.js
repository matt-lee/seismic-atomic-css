import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('docs', function() {
    this.route('colors');
    this.route('typography');
    this.route('borders');
    this.route('spacing');
    this.route('depth');
    this.route('layout');
    this.route('buttons');

    this.route('components', function() {
      this.route('modal');
      this.route('cards');
    });
    this.route('usage');
  });

  this.route('components', function() {});
});

export default Router;
