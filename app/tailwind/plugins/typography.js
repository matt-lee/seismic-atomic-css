import { map } from 'lodash-es';

export default function({ typography, variants }) {
  return function({ e, addUtilities }) {
    const utilities = map(typography, (value, key) => {
      return {
        [`.${e(`text-${key}`)}`]: {
          'line-height': value['line'],
          'font-size': value['size'] + value['unit']
        }
      };
    });

    addUtilities(utilities, variants);
  };
}
