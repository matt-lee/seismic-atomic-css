import { map } from 'lodash-es';

export default function({ gradients, variants }) {
  return function({ addUtilities, e }) {
    const utilities = map(gradients, ([start, end], name) => ({
      [`.bg-gradient-${e(name)}`]: {
        backgroundImage: `linear-gradient(to right, ${start}, ${end})`
      }
    }));

    addUtilities(utilities, variants);
  };
}
