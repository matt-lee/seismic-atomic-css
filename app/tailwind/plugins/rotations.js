import { map } from 'lodash-es';

export default function({ rotations }) {
  return function({ e, addUtilities }) {
    const utilities = map(rotations, (value, key) => {
      return {
        [`.${e(`rotate-${key}`)}`]: {
          transform: `rotate(${value})`
        }
      };
    });

    addUtilities(utilities);
  };
}
