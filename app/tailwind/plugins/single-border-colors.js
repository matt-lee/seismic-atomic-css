export default function({
  directions = ['t', 'r', 'b', 'l'],
  colors,
  variants
}) {
  return function({ addUtilities, e }) {
    const map = {
      t: 'top',
      r: 'right',
      b: 'bottom',
      l: 'left'
    };
    const utilities = {};

    directions.map(direction => {
      Object.keys(colors).forEach(color => {
        const css = {};
        css[`border-${map[direction]}-color`] = color;
        utilities[`.border-${direction}-${color}`] = css;
      });
    });

    addUtilities(utilities, variants);
  };
}
