import { map } from 'lodash-es';

export default function({ carets, variants }) {
  return function({ addUtilities, e }) {
    const utilities = map(carets, (value, name) => ({
      [`.caret-${e(name)}`]: {
        'caret-color': value
      }
    }));

    addUtilities(utilities, variants);
  };
}
