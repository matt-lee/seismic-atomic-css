/*
 |-----------------------------------------------------------------------------
 | Typography
 |-----------------------------------------------------------------------------
 |
 | Here is where you define your typography.
 |
 |
 | Class name: .text-{typography}
 | Applied: font-size, line-height
 |
 */

export default {
  default: { size: 16, line: 1.5, unit: "px" },
  "4xl": { size: 3, line: 1.5, unit: "rem" },
  "3xl": { size: 2.5, line: 1.5, unit: "rem" },
  "2xl": { size: 2, line: 1.5, unit: "rem" },
  xl: { size: 1.5, line: 1.5, unit: "rem" },
  lg: { size: 1.25, line: 1.5, unit: "rem" },
  base: { size: 1, line: 1.5, unit: "rem" },
  sm: { size: 0.875, line: 1.4, unit: "rem" },
  xs: { size: 0.75, line: 1.166667, unit: "rem" },
  "2xs": { size: 0.625, line: 1.4, unit: "rem" }
};
