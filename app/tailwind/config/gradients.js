import colors from './colors';

export default {
  'white-transparent': [colors['white'], colors['transparent']]
};
