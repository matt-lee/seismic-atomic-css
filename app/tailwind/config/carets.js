import colors from './colors';

export default {
  active: colors['blue'],
  alt: colors['white']
};
