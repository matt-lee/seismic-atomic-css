/*
|-------------------------------------------------------------------------------
| Colors                                    https://tailwindcss.com/docs/colors
|-------------------------------------------------------------------------------
| Text colors                         https://tailwindcss.com/docs/text-color
|
| Class name: .text-{color}
|
|-----------------------------------------------------------------------------
| Background colors             https://tailwindcss.com/docs/background-color
|
| Class name: .bg-{color}
|
|-----------------------------------------------------------------------------
| Border colors                     https://tailwindcss.com/docs/border-color
|
| Class name: .border-{color}
|
*/
export default {
  transparent: 'transparent',

  black: '#22292f',
  white: '#ffffff',

  'gray-xl': '#F9F9FB',
  'gray-l': '#EDEDED',
  gray: '#92A2AB',
  'gray-d': '#536872',
  'gray-xd': '#293F4D',

  green: '#13CE66',
  yellow: '#F7CF46',
  red: '#FF4949',
  orange: '#F1592A',
  'orange-d': '#ee440f',

  'blue-l': '#D0E7EF',
  blue: '#1CA1DC',
  'blue-d': '#0076A8',
  'blue-xd': '#293F4D',

  'actual-black': '#000000'
};
