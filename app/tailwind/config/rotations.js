export default {
  '1/4': '90deg',
  '1/2': '180deg',
  '3/4': '270deg'
};
