import colors from "./colors";

/*
|-----------------------------------------------------------------------------
| Text colors                         https://tailwindcss.com/docs/text-color
|-----------------------------------------------------------------------------
|
| Here is where you define your text colors. By default these use the
| color palette we defined above, however you're welcome to set these
| independently if that makes sense for your project.
|
| Class name: .text-{color}
|
*/

export default {
  black: colors["black"],
  orange: colors["orange"],
  white: colors["white"],
  gray: colors["gray"],
  "gray-d": colors["gray-d"],
  "gray-xd": colors["gray-xd"],
  blue: colors["blue"],
  "blue-xd": colors["blue-xd"],
  red: colors["red"],
  green: colors["green"]
};
