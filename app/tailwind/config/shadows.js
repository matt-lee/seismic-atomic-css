/*
|-----------------------------------------------------------------------------
| Shadows                                https://tailwindcss.com/docs/shadows
|-----------------------------------------------------------------------------
|
| Here is where you define your shadow utilities. As you can see from
| the defaults we provide, it's possible to apply multiple shadows
| per utility using comma separation.
|
| If a `default` shadow is provided, it will be made available as the non-
| suffixed `.shadow` utility.
|
| Class name: .shadow-{size?}
|
*/
export default {
  default: '0 2px 8px 0 rgba(0,0,0,0.2);',
  modal: '0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);',
  flyout: '0 0 6px 0 rgba(0,0,0,0.3);'
};
