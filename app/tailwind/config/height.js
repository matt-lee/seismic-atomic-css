import grid from './grid';
import icons from './icons';

/*
|-----------------------------------------------------------------------------
| Height                                  https://tailwindcss.com/docs/height
|-----------------------------------------------------------------------------
|
| Here is where you define your height utility sizes. These can be
| percentage based, pixels, rems, or any other units. By default
| we provide a sensible rem based numeric scale plus some other
| common use-cases. You can, of course, modify these values as
| needed.
|
| Class name: .h-{size}
|
*/
export default Object.assign(grid, icons, {
  auto: 'auto',
  full: '100%',
  screen: '100vh'
});
