import grid from './grid';
import icons from './icons';

export default Object.assign(
  {
    button: '3px'
  },
  grid,
  icons
);
