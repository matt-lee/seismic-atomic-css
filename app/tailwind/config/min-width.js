import grid from './grid';
import screens from './screens';
import icons from './icons';
/*
|-----------------------------------------------------------------------------
| Minimum width                        https://tailwindcss.com/docs/min-width
|-----------------------------------------------------------------------------
|
| Here is where you define your minimum width utility sizes. These can
| be percentage based, pixels, rems, or any other units. We provide a
| couple common use-cases by default. You can, of course, modify
| these values as needed.
|
| Class name: .min-w-{size}
|
*/

export default Object.assign(
  {
    button: '120px',
    'button-lg': '200px',
    flyout: '180px'
  },
  grid,
  screens,
  icons
);
