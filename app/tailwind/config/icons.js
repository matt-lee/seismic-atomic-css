/*
  These are widths / heights that are ment to be mixed into other confugrations

 */
export default {
  'icon-sm': '12px',
  'icon-md': '20px',
  icon: '24px',
  'icon-lg': '32px'
};
