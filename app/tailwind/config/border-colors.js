import colors from "./colors";

/*
|-----------------------------------------------------------------------------
| Border colors                     https://tailwindcss.com/docs/border-color
|-----------------------------------------------------------------------------
|
| Here is where you define your border colors. By default these use the
| color palette we defined above, however you're welcome to set these
| independently if that makes sense for your project.
|
| Take note that border colors require a special "default" value set
| as well. This is the color that will be used when you do not
| specify a border color.
|
| Class name: .border-{color}
|
*/

export default Object.assign({
  default: colors["gray-l"],
  black: colors["black"],
  gray: colors["gray"],
  "gray-xd": colors["gray-xd"],
  orange: colors["orange"],
  "orange-d": colors["orange-d"],
  blue: colors["blue"],
  red: colors["red"],
  green: colors["green"],
  white: colors["white"],
  "gray-xl": colors["gray-xl"],
  "gray-l": colors["gray-l"],
  "blue-xd": colors["blue-xd"],

  transparent: colors["transparent"]
});
