import { map } from 'lodash-es';
import typography from './typography';
let result = {};
const lineHeights = map(typography, (value, key) => {
  result[key] = `${value.line}px`;
});

export default result;
