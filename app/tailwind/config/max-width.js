import grid from './grid';
import screens from './screens';
import icons from './icons';
/*
|-----------------------------------------------------------------------------
| Maximum width                        https://tailwindcss.com/docs/max-width
|-----------------------------------------------------------------------------
|
| Here is where you define your maximum width utility sizes. These can
| be percentage based, pixels, rems, or any other units. By default
| we provide a sensible rem based scale and a "full width" size,
| which is basically a reset utility. You can, of course,
| modify these values as needed.
|
| Class name: .max-w-{size}
|
*/

export default Object.assign(
  {
    button: '120px',
    'button-lg': '200px',
    flyout: '180px'
  },
  grid,
  screens,
  icons
);
